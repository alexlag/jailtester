class CreateSubmissions < ActiveRecord::Migration
  def change
    create_table :submissions do |t|
      t.string :mongoId
      t.string :email
      t.string :filespath
      t.text :stdout
      t.text :stderr
      t.text :log
      t.string :result

      t.timestamps
    end
  end
end
