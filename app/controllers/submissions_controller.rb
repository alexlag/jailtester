class SubmissionsController < ApplicationController

  include FileHelper
  # GET /submissions
  # GET /submissions.json
  def index
    if params[:email]
      @submissions = Submission.find_all_by_email(params[:email])
    else
      @submissions = Submission.all
    end


    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @submissions }
    end
  end

  # GET /submissions/1
  # GET /submissions/1.json
  def show
    @submission = Submission.find(params[:id])
    @files = read_files @submission.filespath
    @description = read_description @submission.filespath

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @submission }
    end
  end

  # GET /submissions/new
  # GET /submissions/new.json
  def new
    @submission = Submission.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @submission }
    end
  end

  # GET /submissions/1/edit
  def edit
    @submission = Submission.find(params[:id])
  end

  # POST /submissions
  # POST /submissions.json
  def create
    unzip = unzip_and_store(params[:submission][:upload], params[:submission][:train], params[:submission][:email])
    result = unzip[:log].empty? ? 'Processing upload' : 'Error occurred'
    @submission = Submission.new(mongoId: params[:submission][:mongoId],
                                 email: params[:submission][:email],
                                 filespath: unzip[:dir],
                                 log: unzip[:log],
                                 result: result
    )

    respond_to do |format|
      if @submission.save
        @submission.delay.run_checker if unzip[:log].empty?
        format.html { redirect_to @submission, notice: 'Submission was successfully created.' }
        format.json { render json: @submission, status: :created, location: @submission }
      else
        format.html { render action: "new" }
        format.json { render json: @submission.errors, status: :unprocessable_entity }
      end
    end
  end

  def custom_create
    unzip = get_and_store(params[:submission][:upload], params[:submission][:train], params[:submission][:email])
    result = unzip[:log].empty? ? 'Processing upload' : 'Error occurred'
    @submission = Submission.new(mongoId: params[:submission][:mongoId],
                                 email: params[:submission][:email],
                                 filespath: unzip[:dir],
                                 log: unzip[:log],
                                 result: result
    )

    respond_to do |format|
      if @submission.save
        @submission.delay.run_checker if unzip[:log].empty?
        format.html { redirect_to @submission, notice: 'Submission was successfully created.' }
        format.json { render json: @submission, status: :created, location: @submission }
      else
        format.html { render action: "new" }
        format.json { render json: @submission.errors, status: :unprocessable_entity }
      end
    end
  end

  def rerun
    @submission = Submission.find(params[:id])
    @submission.delay.run_checker

    redirect_to @submission, notice: 'Rerun in progress'
  end

  # PUT /submissions/1
  # PUT /submissions/1.json
  def update
    @submission = Submission.find(params[:id])

    respond_to do |format|
      if @submission.update_attributes(params[:submission])
        format.html { redirect_to @submission, notice: 'Submission was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @submission.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /submissions/1
  # DELETE /submissions/1.json
  def destroy
    @submission = Submission.find(params[:id])
    @submission.destroy

    respond_to do |format|
      format.html { redirect_to submissions_url }
      format.json { head :no_content }
    end
  end

  def grab_result(sub)
    puts sub
    return 'Not found' if sub.nil?
    return sub.result if sub.email != 'baseline2@ispras.ru'
    format('%.10f', sub.result.to_f + 0.0000000001)
  end

  def submission_status
    submission = Submission.find_by_mongoId(params[:mongoid])
    @result = grab_result submission
    response.headers['Access-Control-Allow-Origin'] = '*'
    respond_to do |format|
      format.html
      format.json { render json: {result: @result} }
    end
  end
end
