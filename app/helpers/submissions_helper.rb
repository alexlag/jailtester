module SubmissionsHelper
  require 'open3'

  def run_code(dir, maxt=6)
    Open3.capture2(Rails.configuration.access_cmd + ' rm -rf ' + Rails.configuration.jail_dir + '/*')
    sleep 3
    Open3.capture2(Rails.configuration.access_cmd + ' cp -r ' + dir + '/* ' + Rails.configuration.jail_dir)
    Open3.capture2(Rails.configuration.access_cmd + ' cp -r ' + Rails.configuration.eval_dir + '/* ' + Rails.configuration.jail_dir)
    sleep 3

    cmd = Rails.configuration.jailbox_cmd
    evaler = Rails.configuration.jail_dir + '/eval.py'
    train_set = Rails.configuration.jail_dir + '/train.json'
    test_set = Rails.configuration.jail_dir + '/test_corpus.json'

    Dir.chdir(Rails.configuration.jail_dir + '/archive'){
      out = run('%s %s %s %s 2>&1' % [cmd, evaler, train_set, test_set], Rails.configuration.jail_dir + '/archive', 20*60)
      if out[:success]
        result = /(?<=Accuracy: )(.*)$/.match(out[:out]).to_s
      else
        result = 'Error occurred'
      end

      {
          stdout: out[:out],
          stderr: out[:errmesg],
          log: out[:out],
          result: result
      }
    }
  end

  def run(s, dir, maxt = 10*60)
    success, errmesg = true, ''
    begin
      pipe = IO.popen(s, chdir: dir)

      begin
        Timeout::timeout(maxt) do |t|
          a = Process.waitpid2(pipe.pid)
          if a[1].exitstatus != 0
            success = false
            errmesg = 'Execution failed'
          end
        end
      rescue Timeout::Error => e
        begin
          Process.kill('TERM', pipe.pid)
        rescue Object => e
          success = false
          errmesg = 'Execution has timed out, no kill'
        end
        success = false
        errmesg = 'Execution has timed out'
      end
    rescue Exception => e
      success = false
      errmesg = 'Execution failed: ' + e.to_s
    end

    out = pipe.gets(nil)
    pipe.close
    {
        success: success,
        errmesg: errmesg,
        out: out.to_s
    }
  end

end
