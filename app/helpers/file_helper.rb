module FileHelper
  require 'open3'
  require 'charlock_holmes'

  def run_with_log(cmd, log)
    o, e, s = Open3.capture3(cmd)
    if s.exitstatus != 0
      log << o + "\n" + e +"\n"
    end
  end

  def unzip_and_store(upload, tweets, id)
    log = ''
    name = upload.original_filename
    dir = Rails.configuration.jail_store + '/%s/%s' % [id, Time.now.to_i]
    directory = dir + '/archive'
    run_with_log('mkdir -p %s' % directory, log)
    path = File.join(directory, 'archive.zip')
    File.open(path, "wb") { |f| f.write(upload.read) }
    run_with_log('unzip %s -d %s' % [path, directory], log)
    run_with_log('rm -f %s' % path, log)
    path = File.join(dir, 'train.json')
    File.open(path, "wb") { |f| f.write(tweets.read) }
    return {
        dir: dir,
        log: log
    }
  end

  def get_and_store(upload, tweets, id)
    log = ''
    dir = Rails.configuration.jail_store + '/%s/%s' % [id, Time.now.to_i]
    directory = dir + '/archive'
    run_with_log('mkdir -p %s' % directory, log)
    path = File.join(directory, 'archive.zip')
    File.open(path, "wb") do |f|
      upload[:data].each { |byte| f.print byte.chr }
    end
    run_with_log('unzip %s -d %s' % [path, directory], log)
    run_with_log('rm -f %s' % path, log)
    path = File.join(dir, 'train.json')
    File.open(path, "w") { |f| f.write(tweets[:data]) }
    return {
        dir: dir,
        log: log
    }
  end

  def read_description(dir)
    result = []
    Dir.glob(dir + '/archive/**/*.txt') do |file|
      content = File.read(file)
      detection = CharlockHolmes::EncodingDetector.detect(content)
      begin
        utf8_encoded_content = CharlockHolmes::Converter.convert content, detection[:encoding], 'UTF-8'
        result << { name: file, content: utf8_encoded_content }
      rescue
        result << { name: file, content: "!!! Couldn't load  file !!!"}
      end
    end
    return result
  end

  def read_files(dir)
    result = []
    Dir.glob(dir + '/archive/**/*.py') do |file|
      result << { name: file, content: IO.read(file) }
    end
    return result
  end

end
