class Submission < ActiveRecord::Base
  include SubmissionsHelper
  attr_accessible :filespath, :email, :log, :mongoId, :stderr, :stdout, :result

  validates_uniqueness_of :mongoId

  def run_checker
    res = run_code(self.filespath)
    self.stdout = res[:stdout]
    self.stderr = res[:stderr]
    self.log += res[:log]
    self.result = res[:result]

    self.save!
  end
end
