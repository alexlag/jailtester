import json

def deserialize(filename):
    data_file = open(filename)
    data = json.load(data_file)
    
    #filter data
    filtered_data = [(obj["text"], obj["polarity"]) for obj in data if obj["denied"]==0]
    
    return filtered_data

#print len(deserialize("../data/tweets.json"))
#for obj in deserialize("../data/tweets.json"):
#    print obj[0], obj[1]