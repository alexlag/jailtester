from deserializer import deserialize
import numpy as np
from SentimentAnalyzer import SentimentAnalyzer
from sklearn.metrics import *
from sklearn.cross_validation import StratifiedKFold

path_to_training_corpus = "checker/src/tweets.json"
path_to_testing_corpus = "checker/src/testing_corpus.json"


def eval(training_corpus, testing_corpus):
    sa = SentimentAnalyzer(training_corpus) #training of the classifier
    #testing classifier
    texts, answers = zip(*testing_corpus)
    pred = sa.getClasses(texts) 
    score = accuracy_score(answers,pred) #use accuracy because of multilabel classification
    #print classification_report(answers,pred) #see precision, recall and F1 scores for each class
    return score

def cross_validation_test(CV_folds, corpus):
    answers = zip(*corpus)[1]
    kf = StratifiedKFold(answers, n_folds=CV_folds)
    scores = []
    for train, test in kf:
        training_corpus = [corpus[i] for i in train]
        testing_corpus = [corpus[i] for i in test]
        score = eval(training_corpus,testing_corpus)
        scores.append(score)
    return scores


# evaluation
#training_corpus = deserialize(path_to_training_corpus)
#testing_corpus = deserialize(path_to_testing_corpus)
#print eval(training_corpus, testing_corpus)

# temporary evaluation by crossvalidation
corpus = deserialize(path_to_training_corpus)
scores = np.array(cross_validation_test(4, corpus))
print("Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))    


